/* ----------------------------------------------------------------*/
/* ----------------------  Calendar Logic -------------------------*/
/* ----------------------------------------------------------------*/


// As of now, the ids of the events are merely their respective index in the
// events array.
// I could have used length of array instead of this 
// counter, but it stays for now. This is because it makes it easier to picture 
// the database operations that would take place instead of the events array.
var idCounter = 0;
var events = [];



// Completely resets the calendar so that it contains no events.
function resetCalendar() {
  idCounter = 0;
  while(events.length > 0) {
    events.pop();
  }
}



// Returns true if the events evt1 and evt2 collide.
function collides(evt1, evt2) {
  if (evt1.start <= evt2.start && evt1.end > evt2.start) {
    return true;
  }
  if (evt1.start >= evt2.start && evt1.start < evt2.end) {
    return true;
  }
  return false;
}



// Returns maximum simultaneous collisions by doing a recursive check for shared
// collisions between events.
function maxCollisions(evt, collisions) {
  
  // Find shared collisions.
  var sharedCollisions = [];
  collisions.forEach(function(c){
    evt.collisions.forEach(function(sc) {
      if (c == sc) {
        sharedCollisions.push(c);
      }
    });
  });
  
  // If shared collisions is zero, then this check did not add to the already
  // known number of simultaneus events.
  if (sharedCollisions.length === 0) {
    return 0;
  }
  
  // Pass the search down to the events that collide with all events that has 
  // been "added to the chain" so far.
  var max = 0;
  for (var i=0, sc=sharedCollisions[i]; i<sharedCollisions.length;
    i++, sc=sharedCollisions[i]) {
    
    // Remove the event of intereset from the list of collisions since it 
    // does not collide with itself anyway.
    var tempCollisions = sharedCollisions.slice(i, sharedCollisions.length);
    var temp = maxCollisions(events[sc], tempCollisions);
    if (temp > max) {
      max = temp;
      if (max == sharedCollisions.length) {
        break;
      }
    }
    
    // If calling this method on an event does not return the maximum number,
    // then the biggest "pile up" of events does not contain the respective event
    // at all, and it can therefore be left out when exploring the other events.
    sharedCollisions = sharedCollisions.slice(i, sharedCollisions.length);
  }
  
  // Return one because the events share at least one collision. Add the maximum
  // number of shared collisions between the two inspected events, and their 
  // shared collisions. (maximum return value of succeeding searches)
  return 1 + max;
}



// Calculates the relative width of each event. If the event does not collide
// with any other this value should be 1 (as in 100%). If it collides with one
// other event, this value should be 1/2 (as in 50%). If it collides with two 
// other events that do not themselves collide, this value should still be 1/2.
// HOWEVER: after initially calculating the maximum width of each event, set the
// width of each event to no more than the width of the events that it collides
// with.
function calcWidthAndPosition(evt) {
  var unAvailableIndexes = [];
  evt.width = evt.maxCollisions;
  evt.collisions.forEach(function(id) {
    var otherEvt = events[id];
    evt.width = evt.width < otherEvt.maxCollisions? 
      otherEvt.maxCollisions : evt.width;
    
    // Keep track of unavailable positional indexes.
    if (typeof otherEvt.pIndex != 'undefined') {
      unAvailableIndexes.push(otherEvt.pIndex);
    }
  });
  evt.width++;
  
  // Set positional index to the smallest available among colliding events.
  if (evt.width == 1) {
    evt.pIndex = 0;
  } else {
    for (var i=0; i < evt.width; i++) {
      if (unAvailableIndexes.indexOf(i) == -1) {
        evt.pIndex = i;
        break;
      }
    }
  }
  
  evt.width = 1 / evt.width;
  
  if (typeof evt.pIndex == 'undefined') {
    console.error('No available pIndex for event!');
  }
}

// Inserts the event into the events array, 
// and calculates new positions and widths for all affected events
function addEvent(evt) {
  
  evt.id = idCounter;
  idCounter++;
  evt.collisions = [];
  
  if (evt.id != events.length) {
    // Read comment above the declaration of idCounter.
    console.error('idCounter is not functioning properly!');
  }
  
  // Calculate new collisions, and store the id of the colliding events in an 
  // array contained in the event they collide with.
  var affectedEvents = [evt.id];
  for (var i=0; i<events.length; i++) {
    var otherEvt = events[i];
    if (collides(evt, otherEvt)) {
      evt.collisions.push(otherEvt.id);
      otherEvt.collisions.push(evt.id);
      affectedEvents.push(otherEvt.id);
    }
  }
  
  // add the event to the events array.
  events.push(evt);
  
  // Calculate maximum simultaneous collisions for each event.
  affectedEvents.forEach(function(id) {
    var evt = events[id];
    evt.maxCollisions = maxCollisions(evt, evt.collisions);
  });
  
  // Set appropriate width for each event.
  // "Alle kolliderende hendelser skal vises med samme bredde som 
  // hendelsene de kolliderer med"
  events.forEach(function(evt) {
    calcWidthAndPosition(evt);
  });
  
}

function removeEvent(id) {
  var evt = events[id];
  
  // Remove stored collisions with this event.
  evt.collisions.forEach(function(otherId){
    var index = events[otherId].collisions.indexOf(id);
    if (index == -1) {
      console.error('Collision is not mutually registered.');
    }
    events[otherId].collisions.splice(index, 1);
  });
  
  //Calculate the new number of maximum collisions for the affected events.
  evt.collisions.forEach(function(id) {
    events[id].maxCollisions = maxCollisions(events[id], events[id].collisions);
  });
  
  // Update id's of events so that they correspond to their index in the array.
  events.splice(id, 1);
  for (var i=id; i<events.length;i++) {
    events[i].id--;
  }
  
  // Update stored collisions with respect to the newly assigned ids above.
  events.forEach(function(evt) {
    for (var i=0; i<evt.collisions.length; i++) {
      if (evt.collisions[i] > id) {
        evt.collisions[i]--;
      }
    }
  });
  
  // Calculate new width and position.
  events.forEach(function(evt) {
    calcWidthAndPosition(evt);
  });
  
  // Decrease idCounter to account for the removed event.
  idCounter--;
}

module.exports = {
    events: events,
    resetCalendar: resetCalendar,
    addEvent: addEvent,
    removeEvent: removeEvent,
};