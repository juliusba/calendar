/* ----------------------------------------------------------------*/
/* --------------------  Testing ----------------------------------*/
/* ----------------------------------------------------------------*/

var calendar = require('./calendar');

function runTests() {

  console.log('Testing...');
  
   /*
  Kalender 1
  Hendelse 1: [Start: 10, Slutt: 11]
  Hendelse 2: [Start: 12, Slutt: 14]
  Hendelse 3: [Start: 13, Slutt: 15]
  */
  
  console.log('Kalender 1...');
  
  console.assert(calendar.events.length === 0);
  calendar.addEvent({'name': 'Hendelse 1', start:10, end:11});
  console.assert(calendar.events.length == 1);
  console.assert(calendar.events[0].width == 1);
  console.assert(calendar.events[0].pIndex === 0);
  calendar.addEvent({'name': 'Hendelse 2', start:12, end:14});
  console.assert(calendar.events.length == 2);
  console.assert(calendar.events[0].width == 1);
  console.assert(calendar.events[0].pIndex === 0);
  console.assert(calendar.events[1].width == 1);
  console.assert(calendar.events[1].pIndex === 0);
  calendar.addEvent({'name': 'Hendelse 3', start:13, end:15});
  console.assert(calendar.events.length == 3);
  console.assert(calendar.events[0].width == 1);
  console.assert(calendar.events[0].pIndex === 0);
  console.assert(calendar.events[1].width == 0.5);
  console.assert(calendar.events[1].pIndex === 0);
  console.assert(calendar.events[2].width == 0.5);
  console.assert(calendar.events[2].pIndex === 1);
  calendar.removeEvent(2);
  console.assert(calendar.events.length == 2);
  console.assert(calendar.events[0].width == 1);
  console.assert(calendar.events[0].pIndex === 0);
  console.assert(calendar.events[1].width == 1);
  console.assert(calendar.events[1].pIndex === 0);
  calendar.removeEvent(1);
  console.assert(calendar.events.length == 1);
  console.assert(calendar.events[0].width == 1);
  console.assert(calendar.events[0].pIndex === 0);
  calendar.removeEvent(0);
  console.assert(calendar.events.length === 0);
  
  /*
  Kalender 2
  Hendelse 1: [Start: 10, Slutt: 13]\
  Hendelse 2: [Start: 11, Slutt: 13]\
  Hendelse 3: [Start: 12, Slutt: 13]\
  */
  
  calendar.resetCalendar();
  
  console.log('Kalender 2...');
  
  console.assert(calendar.events.length === 0);
  calendar.addEvent({'name': 'Hendelse 1', start:10, end:13});
  console.assert(calendar.events.length == 1);
  console.assert(calendar.events[0].width == 1);
  console.assert(calendar.events[0].pIndex === 0);
  calendar.addEvent({'name': 'Hendelse 2', start:11, end:13});
  console.assert(calendar.events.length == 2);
  console.assert(calendar.events[0].width == 1/2);
  console.assert(calendar.events[0].pIndex === 0);
  console.assert(calendar.events[1].width == 1/2);
  console.assert(calendar.events[1].pIndex === 1);
  calendar.addEvent({'name': 'Hendelse 3', start:12, end:13});
  console.assert(calendar.events.length == 3);
  console.assert(calendar.events[0].width == 1/3);
  console.assert(calendar.events[0].pIndex === 0);
  console.assert(calendar.events[1].width == 1/3);
  console.assert(calendar.events[1].pIndex === 1);
  console.assert(calendar.events[2].width == 1/3);
  console.assert(calendar.events[2].pIndex === 2);
  calendar.removeEvent(2);
  console.assert(calendar.events.length == 2);
  console.assert(calendar.events[0].width == 1/2);
  console.assert(calendar.events[0].pIndex === 0);
  console.assert(calendar.events[1].width == 1/2);
  console.assert(calendar.events[1].pIndex === 1);
  calendar.removeEvent(1);
  console.assert(calendar.events.length == 1);
  console.assert(calendar.events[0].width == 1);
  console.assert(calendar.events[0].pIndex === 0);
  calendar.removeEvent(0);
  console.assert(calendar.events.length === 0);
  
  /*
  Kalender 3
  Hendelse 1: [Start: 9, Slutt: 11]
  Hendelse 2: [Start: 10, Slutt: 13]
  Hendelse 3: [Start: 11, Slutt: 14]
  */
  
  calendar.resetCalendar();
  
  console.log('Kalender 3...');
  
  console.assert(calendar.events.length === 0);
  calendar.addEvent({'name': 'Hendelse 1', start:9, end:11});
  console.assert(calendar.events.length == 1);
  console.assert(calendar.events[0].width == 1);
  console.assert(calendar.events[0].pIndex === 0);
  calendar.addEvent({'name': 'Hendelse 2', start:10, end:13});
  console.assert(calendar.events.length == 2);
  console.assert(calendar.events[0].width == 1/2);
  console.assert(calendar.events[0].pIndex === 0);
  console.assert(calendar.events[1].width == 1/2);
  console.assert(calendar.events[1].pIndex === 1);
  calendar.addEvent({'name': 'Hendelse 3', start:11, end:14});
  console.assert(calendar.events.length == 3);
  console.assert(calendar.events[0].width == 1/2);
  console.assert(calendar.events[0].pIndex === 0);
  console.assert(calendar.events[1].width == 1/2);
  console.assert(calendar.events[1].pIndex === 1);
  console.assert(calendar.events[2].width == 1/2);
  console.assert(calendar.events[2].pIndex === 0);
  calendar.removeEvent(2);
  console.assert(calendar.events.length == 2);
  console.assert(calendar.events[0].width == 1/2);
  console.assert(calendar.events[0].pIndex === 0);
  console.assert(calendar.events[1].width == 1/2);
  console.assert(calendar.events[1].pIndex === 1);
  calendar.removeEvent(1);
  console.assert(calendar.events.length == 1);
  console.assert(calendar.events[0].width == 1);
  console.assert(calendar.events[0].pIndex === 0);
  calendar.removeEvent(0);
  console.assert(calendar.events.length === 0);
  
  console.log('All tests passed!');
  
}

runTests();