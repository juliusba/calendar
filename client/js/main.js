function CalController($scope) {
    var socket = io.connect();

    $scope.events = [];
    
    $scope.eventName = '';
    $scope.eventStart = '';
    $scope.eventEnd = '';

    socket.on('connect', function () {});

    socket.on('events', function (data) {
        var events = JSON.parse(data);
        console.log('Received events:', events);
        
        var calWidth = $('#eventContainer').width();
        var calHeight = $('#eventContainer').height();
        events.forEach(function(evt){
            evt.style = 'width:' + (evt.width*calWidth-24) + 'px;';
            evt.style += 'height:' + ((evt.end - evt.start)*calHeight/12-4) + 'px;';
            evt.style += 'top:' + ((evt.start-7)*calHeight/12+1) + 'px;';
            evt.style += 'left:' + (evt.width*calWidth*evt.pIndex+1) + 'px;';
        });
        $scope.events = events;
        $scope.$apply();
        
    });
    
    $scope.send = function send(event, data) {
        console.log('Sending msg to server:', data);
        socket.emit(event, JSON.stringify(data));
    };
    
    $scope.createEvent = function createEvent() {
        var evt = {name:$scope.eventName, start:$scope.eventStart, end:$scope.eventEnd};
        console.log('Creating event:', evt);
        $scope.send('createEvent', evt);
        $scope.name = '';
    };
    
    $scope.removeEvent = function removeEvent(id) {
        console.log('Removing event:', id);
        var msg = {id:id};
        $scope.send('removeEvent', msg);
        $scope.name = '';
    };
}
