/* ----------------------------------------------------------------*/
/* ------------------------  Server -------------------------------*/
/* ----------------------------------------------------------------*/

var calendar = require('./calendar_logic/calendar');

var http = require('http');
var path = require('path');

var socketio = require('socket.io');
var express = require('express');

var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

var sockets = [];

function broadcast(event, data) {
  sockets.forEach(function (socket) {
    socket.emit(event, data);
  });
}

router.use(express.static(path.resolve(__dirname, 'client')));

io.on('connection', function (socket) {
  
  socket.emit('events', JSON.stringify(calendar.events));

  sockets.push(socket);

  socket.on('disconnect', function () {
    sockets.splice(sockets.indexOf(socket), 1);
  });
  
  socket.on('createEvent', function(data) {
    var evt = JSON.parse(data);
    calendar.addEvent(evt);
    broadcast('events', JSON.stringify(calendar.events));
  });
  
  socket.on('removeEvent', function(data) {
    console.log('id:', JSON.parse(data).id);
    calendar.removeEvent(JSON.parse(data).id);
    broadcast('events', JSON.stringify(calendar.events));
  });
});

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Calendar server listening at", addr.address + ":" + addr.port);
  
  
  // Fill the calendar with some interesting stuff.
  calendar.addEvent({'name': 'Hendelse 0', start:9, end:11});
  calendar.addEvent({'name': 'Hendelse 1', start:10, end:13});
  calendar.addEvent({'name': 'Hendelse 2', start:11, end:14});
  calendar.addEvent({'name': 'Hendelse 3', start:8, end:13});
  calendar.addEvent({'name': 'Hendelse 4', start:11, end:13});
  calendar.addEvent({'name': 'Hendelse 5', start:15, end:18});
  calendar.addEvent({'name': 'Hendelse 6', start:10, end:11});
  calendar.addEvent({'name': 'Hendelse 7', start:12, end:14});
  calendar.addEvent({'name': 'Hendelse 8', start:13, end:15});

});